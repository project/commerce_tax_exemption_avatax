<?php

namespace Drupal\commerce_tax_exemption_avatax;

use Drupal\commerce_order\Entity\OrderItemInterface;

/**
 * Utilities used for Avatax exemption.
 */
class Utilities {

  /**
   * Gets the order item index from the Avatax line number.
   *
   * @param \Drupal\commerce_order\Entity\OrderItemInterface $order_item
   *   The order item.
   * @param array $lines
   *   Avatax lines.
   */
  public function getOrderItemIndex(OrderItemInterface $order_item, array $lines) {
    foreach ($lines as $index => $line) {
      if (isset($line['number']) && $order_item->uuid() === $line['number']) {
        return $index;
      }
    }
  }

  /**
   * Generates a reason string for Avatax reporting.
   *
   * @param \Drupal\commerce_tax_exemption\Entity\TaxExemption[] $tax_exemptions
   *   Tax exception entities.
   *
   * @return string
   *   Returns a reason string.
   */
  public function generateReasonsString(array $tax_exemptions) {
    $reasons = [];
    foreach ($tax_exemptions as $tax_exemption) {
      $reasons[] = $tax_exemption->getReason()->__toString();
    }

    return implode(',', $reasons);
  }

  /**
   * Generates a tax exemption code for Avatax reporting.
   *
   * @param \Drupal\commerce_tax_exemption\Entity\TaxExemption[] $tax_exemptions
   *   Tax exemption entities.
   *
   * @return string
   *   The code.
   */
  public function generateExemptionCodeString(array $tax_exemptions) {
    $ids = [];
    foreach ($tax_exemptions as $tax_exemption) {
      $ids[] = $tax_exemption->id();
    }
    return implode(',', $ids);
  }

}
