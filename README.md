CONTENTS OF THIS FILE
---------------------

* Introduction
* Requirements
* Installation
* Configuration
* Maintainers

INTRODUCTION
------------
This module adds [Commerce Tax Exemption] support to the [Commerce Connector for AvaTax] module.


REQUIREMENTS
------------

This module requires the following outside of Drupal core:

* Commerce - https://www.drupal.org/project/commerce
* Commerce Tax Exemption - https://www.drupal.org/project/commerce_tax_exemption
* Commerce Connector for AvaTax - https://www.drupal.org/project/commerce_avatax


INSTALLATION
------------

* As with Drupal Commerce, and its contributed modules, you should install with
  Composer.

```bash
composer require drupal/commerce_tax_exemption_avatax:^1.0
```

CONFIGURATION
-------------

    1. There is no configuration as long as you have AvaTax and Commerce Tax
       Exemption configured and operational.


MAINTAINERS
-----------

* Alexander Sluiter (alexandersluiter)
  https://www.drupal.org/u/alexandersluiter

[Commerce Tax Exemption]: https://www.drupal.org/project/commerce_avatax
[Commerce Connector for AvaTax]: https://www.drupal.org/project/commerce_avatax
